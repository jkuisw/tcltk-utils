#!/bin/bash

newVersion=$1
scriptDir=$(cd "$(dirname $0)" && pwd)
scriptDir=$(readlink -f "$scriptDir")

# shellcheck source=./config.sh
source "$scriptDir/config.sh"

${scriptPath:?}/createVersion.sh \
  -f ${packageDir:?} \
  -b ${productionBranch:?} \
  ${packageName:?} \
  $newVersion
