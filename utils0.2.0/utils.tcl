# package: utils
# description:
#   This packages includes some utility functions.

# Initialize package.
source [file join [file dirname [info script]] "init-package.tcl"]

# Dependencies
package require Tcl 8.4

# Define the namespace.
namespace eval ::utils {
  # Export commands.
  namespace export \
    indentText \
    createLine \
    lIsDuplicate \
    getVectorsDistance \
    lInit \
    lIndexedName \
    lFromNEntry \
    lHexToDec \
    lAppendUnique \
    printArrayToFile

  # Define namespace variables.
  # Nothing for now.
}

# ------------------------------------------------------------------------------
# Indents the given string and returns it.
#
# Parameters:
#   text - [default ""] The text to be indented.
#   count - [default 3] The number of repeating the indent string.
#   indentString - [default " "] The indent string.
#
# Returns: The indented string.
proc ::utils::indentText {{text ""} {count 3} {indentString " "}} {
  return [format "%s%s" [string repeat "$indentString" $count] $text]
}

# ------------------------------------------------------------------------------
# Creates a line of characters and returns it.
#
# Parameters:
#   character - [default "#"] The character of which the line is build.
#   count - [default 70] The number of characters the line consists of.
#
# Returns: The line of characters.
proc ::utils::createLine {{character "#"} {count 70}} {
  return [string repeat "$character" $count]
}

# ------------------------------------------------------------------------------
# Checks if the given element is already in the elements list (and therefore is
# a duplicate).
#
# Parameters:
#   elementsList - The list which will be checked for the duplicate.
#   element - The element.
#
# Returns: true if the element is a duplicate, false otherwise.
proc ::utils::lIsDuplicate { elementsList element } {
  if {[lsearch $elementsList $element] == -1} {
    return false
  } else {
    return true
  }
}

# ------------------------------------------------------------------------------
# Calculates the distance (norm) between two vectors.
# Assumption: The given vectors have the same length.
#
# Parameters:
#   vector1 - The first vector.
#   vector2 - The second vector.
#
# Returns: The distance between two vectors.
proc ::utils::getVectorsDistance { vector1 vector2 } {
  set tmpSum 0.0
  foreach coord1 $vector1 coord2 $vector2 {
    set tmpSum [expr {$tmpSum + pow(($coord1 - $coord2), 2)}]
  }

  return [expr {sqrt($tmpSum)}]
}

# ------------------------------------------------------------------------------
# Creates a new list of the given length and initializes its values to the
# given init value.
#
# Parameters:
#   length - [default 1] The length of the list that should be created.
#   initValue - [default 0] The initial value of the liist entries.
#
# Returns: The created list.
proc ::utils::lInit { {length 1} {initValue 0} } {
  set newList {}

  for {set i 0} {$i < $length} {incr i} {
    lappend newList $initValue
  }

  return $newList
}

# ------------------------------------------------------------------------------
# Creates a list of names with an subsequent index which counts from the
# fromIndex to the toIndex (including).
#
# Parameters:
#   name - [default ""] The name which precedes the index.
#   fromIndex - [default 0] The starting index.
#   toIndex - [default 0] The end index.
#
# Returns: A list of base names with a subsequent index.
proc ::utils::lIndexedName { {name ""} {fromIndex 0} {toIndex 0} } {
  set idxNamesList {}

  for {set i $fromIndex} {$i <= $toIndex} {incr i} {
    lappend idxNamesList "${name}${i}"
  }

  return $idxNamesList
}

# ------------------------------------------------------------------------------
# Creates a list from the n`th element of each list entry in the given list. If
# there is a list entry which lenght is smaller or equal than n it will be
# ignored.
#
# Parameters:
#   sourceList - [default {}] The list from which the elements are taken.
#   n - [default 0] The element index of a list entries element.
#
# Returns: The created list.
proc ::utils::lFromNEntry { {sourceList {}} {n 0} } {
  set newList {}

  foreach elem $sourceList {
    if {[llength $elem] > $n} {
      lappend newList [lindex $elem $n]
    }
  }

  return $newList
}

# ------------------------------------------------------------------------------
# Convert a list of hexadecimal values to a list of decimal values.
#
# Parameters:
#   hexList - [default {}] A list with hexadecimal values.
#
# Returns: The converted list.
proc ::utils::lHexToDec { {hexList {}} } {
  set decList {}

  foreach hexVal $hexList {
    scan $hexVal "%x" decVal
    lappend decList $decVal
  }

  return $decList
}

# ------------------------------------------------------------------------------
# Appends the given values only to the list if the values are not already in the
# list. The parameters are the same as the lappend command.
#
# Parameters:
#   varName - The variable name of the list.
#   args - The values to append.
#
# Returns:
proc ::utils::lAppendUnique { varName args } {
  upvar $varName lst

  foreach value $args {
    if {[lsearch -exact $lst $value] == -1} { lappend lst $value }
  }
}

# ------------------------------------------------------------------------------
# Prints an array to a file.
#
# Parameters:
#   file - A valid file handle which points to the file to print.
#   arrayName - The array name.
#   pattern - [default *] A glob pattern which filter the array entries that are
#     printed to the file.
proc ::utils::printArrayToFile { file arrayName {pattern *} } {
  upvar $arrayName arr
  if {![array exists arr]} {
    catch { close $file }
    error [format "\n%s\n%s%s%s\n" \
      "The procedure \"::utils::printArrayToFile\" failed!" \
      "\"" $arr "\" isn't an array!" \
    ]
  }

  set maxl 0
  set names [lsort [array names arr $pattern]]

  foreach name $names {
      if {[string length $name] > $maxl} {
          set maxl [string length $name]
      }
  }

  set maxl [expr {$maxl + [string length $arrayName] + 2}]

  foreach name $names {
      set nameString [format %s(%s) $arrayName $name]
      puts $file [format "%-*s = %s" $maxl $nameString $arr($name)]
  }
}

# ------------------------------------------------------------------------------
# Creates a *.bat file with the given command to execute.
#
# Parameters:
#   cmd - The command to be executed within the bat file.
#   name - [default "tmpCommand"] The name for the *.bat file (without file
#     extension).
#
# Returns: The path (= directory + file name) to the created *.bat file.
proc ::utils::createCmdBatFile { cmd {name "tmpCommand"}} {
  # First create a bat file with the command to execute.
  set batFilePath [file join [pwd] "${name}.bat"]
  set batFile [open $batFilePath w]

  # Write the commands to the bat file.
  puts $batFile "@ECHO OFF"
  puts $batFile $cmd
  close $batFile

  # Return the file path (= directory + file name).
  return $batFilePath
}

# ------------------------------------------------------------------------------
# Deletes a file if it exists.
#
# Parameters:
#   path - The path (= directory and file name) to the file which should be
#     deleted.
proc ::utils::deleteFileIfExists { path } {
  if {[file exists $path]} { file delete $path }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
