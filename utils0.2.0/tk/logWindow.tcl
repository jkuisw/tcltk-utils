# name: logWindow.tcl
# description: TODO

# ------------------------------------------------------------------------------
# Define dependencies
package require Tk 8.4

# ------------------------------------------------------------------------------
# Global variables
set logServer -1
set windowCount 0
set registeredClientsCount 0

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc logWindowMain { argc argv } {
  global logServer
  global windowCount

  # Parse the arguments.
  if {$argc < 0 || $argc > 3} {
    error \
      "wrong # args: should be \"logWindow.tcl ?windowCount? ?title? ?port?\""
  }

  set windowCount 0
  if {$argc >= 1} {
    set arg [lindex $argv 0]

    if {![string is integer $arg]} {
      error [format "%s\n%s"\
        "windowCount must be an integer" \
        "wrong # args: should be \"logWindow.tcl ?windowCount? ?title? ?port?\""
      ]
    }

    set windowCount $arg
  }

  set title "Logging Window"
  if {$argc >= 2 && [lindex $argv 1] != ""} {
    set title [lindex $argv 1]
  }

  set serverPort [expr {33000 + $windowCount}]
  if {$argc >= 3} {
    set arg [lindex $argv 2]

    if {![string is integer $arg] || $arg < 33000} {
      error [format "%s\n%s"\
        "port must be an integer greater than 32999" \
        "wrong # args: should be \"logWindow.tcl ?windowCount? ?title? ?port?\""
      ]
    }

    set serverPort $arg
  }

  # Initialize GUI.
  initilaizeGui $windowCount $title

  # Initialize log server.
  set logServer [initilaizeLogServer $serverPort]
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc initilaizeLogServer { {port 33000} } {
  global windowCount
  set logServer [socket -server logServerOpenConnection -myaddr localhost $port]

  # Configure the server to be non blocking.
  fconfigure $logServer -blocking No

  puts "Log server of log window $windowCount started on port $port."

  # Inform caller that server is initialized.
  set initInfo [socket -myaddr localhost localhost [expr {$port + 1}]]
  set len [gets $initInfo line] ;# Wait for response of caller to continue.
  close $initInfo
  return $logServer
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc logServerOpenConnection { channel address port } {
  global registeredClientsCount
  global windowCount

  # Configure the channel to be non blocking.
  fconfigure $channel -blocking No

  # When a new log message is available, read it.
  fileevent $channel readable "readLogMessage $channel"
  incr registeredClientsCount
  puts "New client has registered to log server of log window $windowCount."
  puts "Client data:"
  puts "  -> channel: $channel"
  puts "  -> address: $address"
  puts "  -> port: $port"
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc readLogMessage { channel } {
  global registeredClientsCount
  global windowCount

  if {[eof $channel]} {
    # Close the connection to the client.
    close $channel
    set registeredClientsCount [expr {$registeredClientsCount - 1}]
    puts [format \
      "Client of log server for log window %d with channel %s has closed." \
      $windowCount $channel \
    ]

    if {$registeredClientsCount <= 0} {
      # This was the last client, therefore exit the application.
      exitLogWin
    }
  } else {
    writeToLog [read $channel]
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc initilaizeGui { {windowCount 0} {title "Logging Window"} } {
  # Set window title.
  wm title . $title

  # Bind to window destroy event.
  bind . <Destroy> exitLogWin

  # Calculate window geometry.
  set topBottomMargin 64
  set screenheight [winfo screenheight .]
  set useableScreenheight [expr {$screenheight - (2 * $topBottomMargin)}]
  set useableScreenwidth [winfo screenwidth .]

  # Calculate window width.
  set width [expr {($useableScreenwidth / 2) - 4}]
  if {$useableScreenwidth < 1500} {
    set width [expr {$useableScreenwidth - 4}]
  }

  # Calculate window height.
  set height [expr {($useableScreenheight / 3) - 4}]

  # Calculate window x and y position.
  if {$useableScreenwidth < 1500} {
    switch -exact -- [expr {$windowCount % 3}] {
      1 {
        set xpos 0
        set ypos [expr {$topBottomMargin + $height}]
      }
      2 {
        set xpos 0
        set ypos [expr {($topBottomMargin + $height) * 2}]
      }
      0 -
      default {
        set xpos 0
        set ypos 0
      }
    }
  } else {
    switch -exact -- [expr {$windowCount % 6}] {
      1 {
        set xpos $width
        set ypos 0
      }
      2 {
        set xpos 0
        set ypos [expr {$topBottomMargin + $height}]
      }
      3 {
        set xpos $width
        set ypos [expr {$topBottomMargin + $height}]
      }
      4 {
        set xpos 0
        set ypos [expr {($topBottomMargin + $height) * 2}]
      }
      5 {
        set xpos $width
        set ypos [expr {($topBottomMargin + $height) * 2}]
      }
      0 -
      default {
        set xpos 0
        set ypos 0
      }
    }
  }

  # Set window geometry.
  wm geometry . "${width}x${height}+${xpos}+${ypos}"

  # Create log text widget and scrollbar widget.
  text .log -state disabled
  scrollbar .logScrollbar -orient vertical

  # Configure them.
  .log configure -yscrollcommand {.logScrollbar set}
  .logScrollbar configure -command {.log yview}

  pack .logScrollbar -fill y -side right
  pack .log -fill both -expand yes
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc exitLogWin {} {
  global windowCount
  global logServer

  # Close log server.
  if {$logServer != -1} { close $logServer }

  puts "Exit log window $windowCount."
  exit 0
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc writeToLog { msg } {
  .log configure -state normal
  .log insert end $msg
  .log see end
  .log configure -state disabled
}

# ------------------------------------------------------------------------------
# Start logWindowMain procedure.
logWindowMain $argc $argv














#
