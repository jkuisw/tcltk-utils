# package: utils
# description:
#   This packages includes some functions for parallel execution.

# Initialize package.
source [file join [file dirname [info script]] "init-package.tcl"]

# Dependencies
package require Tcl 8.4

# Define the namespace.
namespace eval ::parallel {
  # Export commands.
  namespace export \
    pexec

  # Define namespace variables.
  variable redirectCache
  array set redirectCache {}

  variable scriptDir [file normalize [file dirname [info script]]]

  variable isServerInitialized 0

  # Array to register pending operations.
  variable pendingOperations
  array set pendingOperations {}
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   commandsList - Is a list in the following format:
#     {
#       { <cmd 1 description> <unique operation name> <cmd 1> }
#       { <cmd 2 description> <unique operation name> <cmd 2> }
#       ...
#     }
#     Where <cmd i description> is a short string that describes what the
#     command does and <cmd i> is a string (or list) with the command and its
#     arguments. Furthermore the <unique operation name>, must be a valid TCL
#     variable name. This string is used to synchronize the parallel processes
#     at the end, so that the procedure returns only when all commands finished
#     executing.
#
# Returns:
proc ::parallel::pexec { commandsList { logCommand ""} } {
  global tcl_platform
  variable scriptDir
  variable isServerInitialized
  variable pendingOperations

  if {$logCommand == ""} { set logCommand "puts" }

  set i 0
  foreach { cmdDescription uniqueOpName cmd } $commandsList {
    set logPort [expr {33000 + $i}]
    set logWindow [file join $scriptDir "tk" "logWindow.tcl"]

    # Open new log window.
    # Create server to get notified when log window initialization has finished.
    set isServerInitialized 0
    set waitInitServer [socket \
      -server ::parallel::logWindowInitialized \
      -myaddr localhost \
      [expr {$logPort + 1}]
    ]
    # Start log window.
    exec wish $logWindow $i $cmdDescription $logPort &
    # Wait for the log window to be initialized.
    vwait ::parallel::isServerInitialized
    # Log window is initilaized, therefore close the waitInitServer.
    close $waitInitServer

    # This kicks MS-Windows machines for this application
    after 120 update

    # Create socket connection to log window and configure it non blocking.
    set logSocket [socket -myaddr localhost localhost $logPort]
    fconfigure $logSocket -blocking No

    # Start the command and redirect it's output.
    set batFilePath ""
    if {[string match -nocase "*windows*" $tcl_platform(platform)]} {
      # For windows the command has to be wrapped into a *.bat file, which will
      # be executed than. Otherwise there would be some weird problems (e.g.:
      # redirection of stdout not working properly).

      # First create a bat file with the command to execute.
      set batFilePath [::utils::createCmdBatFile $cmd "tmpCommand${i}"]

      # Now execute the bat file with the command.
      set cmdChannel [open |[auto_execok $batFilePath]]
    } elseif {[string match -nocase "*unix*" $tcl_platform(platform)]} {
      # On unix platforms all should work as expected.
      set cmdChannel [open "| $cmd" r]
    } else {
      error [format "Unsupported platform \"%s\", " \
        $tcl_platform(platform) \
        "only \"unix\" and \"windows\" are supported by this command." \
      ]
    }

    # Register a new pending operation and redirect output to log socket.
    set pendingOperations($uniqueOpName) "running"
    fconfigure $cmdChannel -blocking No
    fileevent $cmdChannel readable [list ::parallel::redirectToChannel \
      $cmdChannel $logSocket $logCommand \
      [list ::parallel::cleanUpPendingOps $uniqueOpName $batFilePath] \
    ]

    incr i
  }

  # Wait for the commands to finish before returning.
  foreach { cmdDescription uniqueOpName cmd } $commandsList {
    if {$pendingOperations($uniqueOpName) != "finished"} {
      vwait ::parallel::pendingOperations($uniqueOpName)
      array unset pendingOperations $uniqueOpName
    }
  }
}

# ------------------------------------------------------------------------------
# FOR INTERNAL USE ONLY! Called when a pending operation has finished executing.
# Deletes the *.bat file if any exists and sets the pending operation to
# finished, so that the executor who started the operation can continue.
#
# Parameters:
#   opName - The name of the pending operation.
#   batFilePath - Path to the probably created *.bat file.
proc ::parallel::cleanUpPendingOps { opName batFilePath } {
  variable pendingOperations

  # Set pending operation to finished.
  set pendingOperations($opName) "finished"

  # Delete *.bat file, because it's not needed anymore.
  ::utils::deleteFileIfExists $batFilePath
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::parallel::logWindowInitialized { channel address port } {
  variable isServerInitialized

  # Log window is now initialized, inform log window that client has received
  # the message and close the channel immediately.
  puts $channel "ok" ;# Confirmation message for the log window.
  flush $channel
  close $channel

  set isServerInitialized 1
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
proc ::parallel::redirectToChannel {
  channelFrom
  channelTo
  {logCommand ""}
  {cleanUp ""}
} {
  variable redirectCache

  # Create redirect cache for channel if not existing.
  if {![info exists redirectCache($channelTo)]} {
    set redirectCache($channelTo) ""
  }

  if {[eof $channelFrom]} {
    # Source channel has closed, therefore close redirection.
    close $channelFrom
    if {$channelTo != "stdout"} { close $channelTo }

    if {$cleanUp != ""} { eval $cleanUp }
    if {$logCommand != ""} { eval $logCommand {$redirectCache($channelTo)} }

    array unset redirectCache $channelTo
  } else {
    catch {
      set str [read $channelFrom]
      append redirectCache($channelTo) $str
      puts $channelTo $str
      flush $channelTo
    }
  }
}
