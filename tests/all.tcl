package require tcltest

tcltest::configure -testdir [file dirname [file normalize [info script]]]
eval tcltest::configure $argv

# Create file for tracking total number of failed tests.
set fid [open "totalFailedCount.tcl" w]
puts $fid "set totalFailedCount 0"
close $fid

# Hook to determine if any of the tests failed. Then we can exit with
# proper exit code: 0=all passed, 1=one or more failed
proc tcltest::cleanupTestsHook {} {
    variable numTests

    # Update total failed count.
    if {$numTests(Failed) > 0} {
      source "totalFailedCount.tcl"
      set totalFailedCount [expr {$totalFailedCount + $numTests(Failed)}]
      set fid [open "totalFailedCount.tcl" w]
      puts $fid "set totalFailedCount $totalFailedCount"
      close $fid
    }
}

tcltest::runAllTests

# Get total failed count, set exit code and delete file "totalFailedCount.tcl".
source "totalFailedCount.tcl"
if {![info exists exitCode]} { set exitCode 0 }
if {$totalFailedCount > 0} { set exitCode 1 }
file delete "totalFailedCount.tcl"

exit $exitCode
